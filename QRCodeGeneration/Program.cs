﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCodeGeneration
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileLocation = @"C:\Users\Mohammad\Downloads\bitmap_dump\Mar8.csv";
            List<string> listOfStrings = CSVReader.ReadCsv(fileLocation);
            foreach (var item in listOfStrings)
            {                
                QRCodeGen.GenerateQR(400, 400, item.ToString());
            }
            
        }
    }
}
