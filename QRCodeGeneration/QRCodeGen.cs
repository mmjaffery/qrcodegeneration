﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using ZXing;
using ZXing.QrCode.Internal;
using ZXing.Rendering;

namespace QRCodeGeneration
{
    public static class QRCodeGen
    {
        private static string imagePath = @"C:\Users\Mohammad\Downloads\bitmap_dump\logo.jpg";
        private static string stringTextToEncode;
        private static string saveLocation = @"C:\Users\Mohammad\Downloads\bitmap_dump\";
        private static int size = 400;

        public static Bitmap GenerateQR(int width, int height, string text)
        {
            var bw = new ZXing.BarcodeWriter();
            var encOptions = new ZXing.Common.EncodingOptions
            {
                Width = width,
                Height = height,
                Margin = 0,
                PureBarcode = false
            };

            encOptions.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

            bw.Renderer = new BitmapRenderer();
            bw.Options = encOptions;
            bw.Format = ZXing.BarcodeFormat.QR_CODE;
            Bitmap bm = bw.Write(text);
            Bitmap overlay = new Bitmap(imagePath);

            int deltaHeigth = bm.Height - overlay.Height;
            int deltaWidth = bm.Width - overlay.Width;
            Graphics g = Graphics.FromImage(bm);
            g.DrawImage(overlay, new Point(deltaWidth / 2, deltaHeigth / 2));

            string trimmed = String.Concat(text.Where(c => !Char.IsWhiteSpace(c)));
            string illegal = trimmed;
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            foreach (char c in invalid)
            {
                illegal = illegal.Replace(c.ToString(), "");
            }


            bm.Save(saveLocation + illegal + ".jpg");

            return bm;

        }
    }
}
