﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QRCodeGeneration
{
    public static class CSVReader
    {
        internal static List<string> ReadCsv(string fileLocation)
        {
            List<string> listA = new List<string>();
            using (var reader = new StreamReader(fileLocation))
            {
                
                //List<string> listB = new List<string>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    //var values = line.Split(',');
                    listA.Add(line);
                    //listA.Add(values[0]);
                    //listB.Add(values[1]);
                }
            }
            return listA;
        }
    }
}
